package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class StudentList {

	private List<Student> l;

	public StudentList() {
		super();
		
		l =new ArrayList<Student>();
		
		l.add(new Student("samson1","1",21));
		l.add(new Student("samson2","2",21));
		l.add(new Student("samson3","3",21));
		l.add(new Student("samson4","4",21));
		
		
	}

	public List<Student> getList() {
		return l;
	}

	public void setList(List<Student> l) {
		this.l = l;
	}
	
		
	
}
