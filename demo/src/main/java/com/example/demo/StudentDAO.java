package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentDAO {

	
	@Autowired
	StudentList l;
	
	public List<Student> getList(){
		
		
		return l.getList();
	}
	
	public void setList(List<Student> l) {
		this.l.setList(l);
	}
}
