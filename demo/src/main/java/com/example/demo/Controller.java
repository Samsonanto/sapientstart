package com.example.demo;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

	
	private static final Logger LOGGER = Logger.getLogger(Student.class);
	
	@Autowired
	StudentDAO sDAO;
	@RequestMapping(value = "/")
	   public String hello() {
	      return "Hello world";
	   }
	@RequestMapping(value = "/Student")
	public List<Student> getStudent(){
		return sDAO.getList();
	}
	
	@RequestMapping("/Student/{name}")
	public Student getDetails(@PathVariable("name") String s) {
		
		LOGGER.info("GET / /Student/"+s);
		
		return (sDAO.getList().stream().filter(x -> x.getName().equals(s) ).collect(Collectors.toList())).get(0);
		
	}
	
	
	@RequestMapping(value = "/Student",method = RequestMethod.POST)
	public String addStudent(@RequestBody Student s) {
		
		sDAO.getList().add(s);
		return "Added Student Sucessfully";
		
	}
	
	@RequestMapping(value = "/Student",method = RequestMethod.PUT)
	public String updateStudent(@RequestBody Student s) {
		
		sDAO.getList().stream().filter(e -> e.getName().equals(s.getName())).forEach(e -> {
			
			e.setAge(s.getAge());
			e.setCity(s.getCity());
			e.setName(s.getName());
			
		});
		return "Updated sucessfully";
	}
	
	@RequestMapping(value = "/Student" , method = RequestMethod.DELETE)
	public String deleteStudent(@RequestParam("name") String name ) {
		
		sDAO.setList( sDAO.getList().stream().filter(e -> !e.getName().equals(name)).collect(Collectors.toList()));
		return "Delete sucessful ";
	}
	
}
