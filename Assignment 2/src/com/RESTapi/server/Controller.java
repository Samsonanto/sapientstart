package com.RESTapi.server;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.RESTapi.dao.UserDAO;
import com.RESTapi.exception.InvalidUserException;
import com.RESTapi.others.RBundle;


@Path("/home")
public class Controller {

	@GET  
	@Produces(MediaType.TEXT_PLAIN) 
	public String welcome() {
		return "Welcome to My RESTapi for Restorent Table reservation";
	}
	
	@POST
	@Path("/api")
	public Response findRestorent(
			@FormParam("long") String lng,
			@FormParam("lat") String lat
			)
	{
		
		Client client = ClientBuilder.newClient();
		WebTarget myResource = client.target("https://developers.zomato.com/api/v2.1/geocode?lat="+lat+"&lon="+lng);
		String response = myResource.request(MediaType.TEXT_PLAIN)
		        .header("user-key", "d983aae7527dd2fa35fe451b6302453f")
		        .get(String.class);
		JSONParser jp =new JSONParser();
		JSONObject jo=null;
		JSONArray arr = null;
		String res = "";
		Map<String,String> map = new TreeMap<String,String>();
		try {
			jo = (JSONObject)jp.parse(response);
			arr = (JSONArray)jo.get("nearby_restaurants");
			Iterator i = arr.iterator();
			
			while(i.hasNext()) {

				Map tmp = (Map)i.next();
				Map restaurant = (Map)tmp.get("restaurant");
				
				String name = (String) restaurant.get("name");
				String loc = restaurant.get("location").toString();
				String url = (String) restaurant.get("url");
				
				res += "[ name : "+name+", loaction : "+loc+", url : "+url+" ]\n";
				
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return Response.status(200).entity(res).build();
	}
	
}
