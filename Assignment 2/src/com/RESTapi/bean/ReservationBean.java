package com.RESTapi.bean;

import java.util.Date;

public class ReservationBean {
	
	private String id ;
	private String resto_name;
	private Date date;
	private String seats;
	public ReservationBean(String id, String resto_name, Date date, String seats) {
		super();
		this.id = id;
		this.resto_name = resto_name;
		this.date = date;
		this.seats = seats;
	}
	public ReservationBean() {
		super();
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getResto_name() {
		return resto_name;
	}
	public void setResto_name(String resto_name) {
		this.resto_name = resto_name;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getSeats() {
		return seats;
	}
	public void setSeats(String seats) {
		this.seats = seats;
	} 
	
	
}
