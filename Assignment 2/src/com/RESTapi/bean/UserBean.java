package com.RESTapi.bean;

public class UserBean {

	private String uname;
	private String eid;
	private String name;
	private String pass;
	private String id;
	private ReservationBean reserve;
	public UserBean(String uname, String eid, String name, String pass, String id) {
		super();
		this.uname = uname;
		this.eid = eid;
		this.name = name;
		this.pass = pass;
		this.id = id;
	}
	public UserBean() {
		super();
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public String getEid() {
		return eid;
	}
	public void setEid(String eid) {
		this.eid = eid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}	
}
