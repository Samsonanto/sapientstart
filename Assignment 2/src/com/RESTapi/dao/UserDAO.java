package com.RESTapi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class UserDAO {
	
	public static boolean validate(String id,String pass) throws Exception
	{
		Connection con = DBConnect.getConnection();
		PreparedStatement ps =con.prepareStatement("select * from rest_users where id = ? and pass = ?");
 		ps.setString(1, id);
 		ps.setString(2, pass);
		
 		
 		ResultSet rs = ps.executeQuery();
 		
 		if(rs.next()) {
 			return true;
 		}
 	
 		return false;	
		
	}
}
