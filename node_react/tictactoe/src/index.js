import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';



const Square = (props) => {
    return (
      <button className="square" onClick={props.onClick}>
         {props.value}
      </button>
    );
}

const Board = (props)=> {
  
  
  const renderSquare = (i)=>{
    return <Square value={props.squares[i]} 
             onClick={()=>props.onClick(i)}/>
  }
   return (
      <div>
        <div className="status"></div>
        <div className="board-row">
          {renderSquare(0)}
          {renderSquare(1)}
          {renderSquare(2)}
        </div>
        <div className="board-row">
          {renderSquare(3)}
          {renderSquare(4)}
          {renderSquare(5)}
        </div>
        <div className="board-row">
          {renderSquare(6)}
          {renderSquare(7)}
          {renderSquare(8)}
        </div>
      </div>
    );
  }

class Game extends React.Component {
  
  constructor(props){
    super(props);
    this.state = {
      history : [{
      sq : Array(9).fill(null),
    }],
      step : 0,    
      xIsNext : true
    }
  }
  calcWinner = (sq) => {
    const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
   ];
    
    for(let i =0;i<lines.length;i++){
      const [a,b,c] = lines[i];
      if(sq[a] && sq[a] === sq[b] && sq[a] === sq[c]){
        return sq[a];
      }
    }
    return null;
  }
  
  clickHandle = (i)=>{

    let history = this.state.history.slice(0,this.state.step+1);
    let len = history.length;

    let sq = [...(history[len-1].sq)];
    
    if(this.calcWinner(sq) || sq[i])
      {
        return ;
      }
    
    sq[i] = this.state.xIsNext ? 'X' : 'O';
    this.setState({history : history.concat([{sq:sq}]),
                   xIsNext : !this.state.xIsNext,
                   step : history.length
                  });
    console.log(this.state.history);
  }
  

  jumpTo = (e)=>{
    this.setState({
      step : e,
      xIsNext : e % 2 ? false:true
    })
  }
  
  render() {
    const h = this.state.history.slice(0,this.state.step+1);
    const current = h[this.state.step];
    const winner = this.calcWinner(current.sq);
    let status;
    const moves = this.state.history.map( (step,move) => {
      
        const desc = move ? "Goto step "+move : "Goto start";
      
     return (
        <li key={move}>
          <button key={move} onClick={()=>{this.jumpTo(move)}} >{desc}</button>
        </li>
      );

      
    });
  
    if(winner){
      status = 'winner : '+ winner;
    }else{
     status = 'Next player: '+(this.state.xIsNext?'X' : 'Y'); 
    }
    
    
    return (
      <div className="game">
        <div className="game-board">
          <Board squares={current.sq} 
            onClick= {this.clickHandle}
            />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <ol>
            {moves}
          
          </ol>
        </div>
      </div>
    );
  }
}




// ========================================

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);
