import React,{Component} from 'react';
import TableHeader from './TableHeader/TableHeader';
import TableContent from './TableContent/TableContent';
import Form from './Form/Form';
import axios from 'axios';
import { CONNREFUSED } from 'dns';
const URL = 'http://localhost:8900/';
class Student  extends Component{
   
   constructor(props){
    super(props);   
    this.state = {
            Student : [],
            regno : undefined,
            lname : undefined,
            fname : undefined,
            dob : undefined,
            city : undefined,
            regnoError : undefined,
            lnameError : undefined,
            fnameError : undefined,
            cityError : undefined,
            dobError :undefined
        } 
   }
   


    componentDidMount() {
        
        axios.get(URL+'Student')
      .then(response => response.data).then( (data) => {
        // handle success
        let arr = [];
        console.log(data);
        for(let val=0;val< data.length;val++){
            
            console.log(val);
            let tmp = {
                regno : data[val].regno,
                lname : data[val].lname,
                fname : data[val].fname,
                dob : data[val].dob,
                city : data[val].citycode
            };
                console.log();
            arr.push(tmp);
        }
        console.log(arr);
        this.setState({ Student : arr });
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .finally(function () {
        // always executed
      });
        
      }
    // nameHandler = (event)=>{

    //     this.setState({
    //         name : event.target.value
    //     });

    // }
    // ageHandler = (event)=>{

    //     this.setState({
    //         age : event.target.value
    //     });

    // }

    
    
    // cityHandler = (event)=>{

    //     this.setState({
            
    //         city : event.target.value
    //     });

    // }

    changeHandler = event => {

        let {name,value} = event.target; 

        console.log(name+" "+value);

        this.setState({
            [name]: value
        });
    }

    stateHandler = (event)=>{

        let flag = true;
        if(!this.state.regno){
            this.setState({
                regnoError : "Empty Feild not accepted"
            })
            flag = false;
        }else{
            this.setState({
                regnoError : undefined
            })
        }
        if(!this.state.fname){
            this.setState({
                fnameError : "Empty Feild not accepted"
            })
            flag = false;
        }else{
            this.setState({
                fnameError : undefined
            })
        }
        if(!this.state.lname){
            this.setState({
                lnameError : "Empty Feild not accepted"
            })
            flag = false;
        }else{
            this.setState({
                lnameError : undefined
            })
        }
         if(!this.state.dob){
            this.setState({
                dobError : "Empty Feild not accepted"
            })
            flag = false;

        } else{
            this.setState({
                dobError : undefined
            })
        }if(!this.state.city){
            this.setState({
                cityError : "Empty Feild not accepted"
            })
            flag = false;

        }else{
            this.setState({
                cityError : undefined
            })
        }
        if(flag){
            
            let tmp = this.state.Student;
            let student = {
                regno : this.state.regno,
                fname : this.state.fname,
                lname : this.state.lname,
                dob : this.state.dob,
                citycode : this.state.city
            };
            axios.post(URL+'Student', student)
              .then(function (response) {
                console.log(response);
              })
              .catch(function (error) {
                console.log(error);
              }).finally(()=>{
                  

                    student ={
                        regno : student.regno,
                    fname : student.fname,
                    lname : student.lname,
                    dob : student.dob,
                    city : student.citycode
                    }
                  tmp.push(student);
          
                  this.setState({Student : tmp});
          
          
                  this.setState({
                      fname : "",
                      lname : "",                
                      dob : "",
                      city : "",
                      regno : ""
                  });
              });

            
        }


    }
    render(){

        return (
            <div className="container">

                <table className="table">
                    <TableHeader />
                    <tbody>
                        <TableContent value = {this.state.Student} />
                        <tr>
                            <td>{this.state.regno}</td>
                            <td>{this.state.fname}</td>
                            <td>{this.state.lname}</td>
                            <td>{this.state.city}</td>
                            <td>{this.state.dob}</td>
                        </tr>

                    </tbody>


                </table>

                <Form 
                fname={this.state.fname} formHandler={this.changeHandler} 
                dob={this.state.dob}   
                city={this.state.city} 
                lname={this.state.lname} 
                regno={this.state.regno}
                stateChange={this.stateHandler}
                fnameError = {this.state.fnameError}  
                lnameError = {this.state.lnameError}  
                cityError = {this.state.cityError}  
                dobError = {this.state.dobError}  
                regnoError = {this.state.regnoError}  
                />

            </div>
        ) ;

    }

}


export default Student;