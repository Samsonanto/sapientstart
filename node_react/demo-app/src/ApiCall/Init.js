import axios from 'axios'; 

let init = () => {

    let data =undefined;
    
    axios.get('http://localhost:8900/Student')
      .then(function (response) {
        // handle success
        console.log(response.data);
        data = response.data;
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .finally(function () {
        // always executed
      });
    
      return data;
    }

export default init;