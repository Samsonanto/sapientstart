import React from 'react'

export default (props) => {

    return (
        props.value.map( (student) => {
            const {regno,fname,lname,dob,city} = student ;
            return (
            <tr>
                <td>{regno}</td>
                <td>{fname}</td>
                <td>{lname}</td>
                <td>{city}</td>
                <td>{dob}</td>
             </tr>
            );

        } )
    );

}