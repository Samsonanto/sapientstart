import React from 'react';
import './Form.css'

export default (props) => {

    return (
  <div>
    <div className="form-group">
        <label forhtml="regno">Registeration Number</label>
        <div className="form-inline">
          <input type="text" className="form-control" id="regno" name="regno" aria-describedby="emailHelp" value={props.regno} onChange={props.formHandler} /> 
          <p className="error"> {props.regnoError}</p>
        </div>
        {/* <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small> */}
    </div>
      <div className="form-group">
        <label forhtml="fname">First Name</label>
        <div className="form-inline">
          <input type="text" className="form-control" id="fname" name="fname" value={props.fname} onChange={props.formHandler} />
          <p className="error"> {props.fnameError}</p>
        </div>
      </div>
      <div className="form-group">
        <label forhtml="lname">Last Name</label>
        <div className="form-inline">
          <input type="text" className="form-control" id="lname" name="lname" value={props.lname} onChange={props.formHandler} />
          <p className="error"> {props.dobError}</p>
        </div>
      </div>
      <div className="form-group">
        <label forhtml="city">City Code</label>
        <div className="form-inline">
          <input type="number" className="form-control" id="city" name="city" value={props.city} onChange={props.formHandler} />
          <p className="error"> {props.cityError}</p>
        </div>
      </div>
      <div className="form-group">
        <label forhtml="dob">Date Of Birth</label>
        <div className="form-inline">
          <input type="date" className="form-control" id="dob" name="dob" value={props.dob} onChange={props.formHandler} />
          <p className="error"> {props.dobError}</p>
        </div>
      </div>
    <button className="btn btn-primary" onClick={props.stateChange}>Submit</button>
</div>
    );

}