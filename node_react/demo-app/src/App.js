import React, { useState } from 'react';
import './App.css';
import  InputUser from  './InputUser/InputUser' ;
import OutputUser from './OutputUser/OutputUser';
import axios from 'axios';

function App() {

  const [appState,appStateSet] = useState( {inputState :  "empty" });
  const [tmpState,tmpStateSet] = useState( {tmp : {}} );

  const changeHandler = (event) => {

    appStateSet({
      
      inputState: event.target.value

    });

  }
 
const  apiCall = ()=>{

  console.log("here");

  axios.get('http://localhost:8900/Student')
  .then(function (response) {
    // handle success

    tmpStateSet({tmp : response});
    console.log(response);
  })
  .catch(function (error) {
    // handle error
    console.log(error);
  })
  .finally(function () {
    // always executed
  });
  }
  return (
    <div className="App container">
     

      <div class="jumbotron jumbotron-fluid">
  <h1 class="display-4">Hello, world!</h1>
  <p class="lead">Your updated text will be seen here : <OutputUser value={appState.inputState} />
</p>
  <hr class="my-4"/>
  <InputUser func = {changeHandler} value={appState.inputState} />
    <button onClick={apiCall}>Temp</button>
    <p>{JSON.stringify(tmpState.tmp)}</p>
</div>
    </div>

    

  );
}

export default App;
