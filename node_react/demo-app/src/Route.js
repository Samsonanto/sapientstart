import React from 'react'
import ReactDOM from 'react-dom'
import { Route, Link, BrowserRouter as Router } from 'react-router-dom'
import App from './App'
import Student from './Student'

const navbar  = (

    <Router>
<nav className="navbar navbar-expand-lg navbar-dark bg-dark">
  <a className="navbar-brand" href="#">React App</a>
  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>

  <div className="collapse navbar-collapse" id="navbarSupportedContent">
    <ul className="navbar-nav mr-auto">
      <li className="nav-item active">
      <a className="nav-link" href="/">Home <span className="sr-only">(current)</span></a>
      </li>
      <li className="nav-item">
      <a className="nav-link" href="/student">Student</a>
      </li>
    </ul>
  </div>
</nav>
    <Route exact path="/" component={App} />
        <Route path="/student" component={Student} />
</Router>
);


const routing = (
    <Router>
      <div>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/student">Student</Link>
          </li>
        </ul>
        <Route exact path="/" component={App} />
        <Route path="/student" component={Student} />
      </div>
    </Router>
  );
  
 export default ReactDOM.render(navbar, document.getElementById('root'))