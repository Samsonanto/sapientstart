import React from 'react';


export default (props) => {

    return (
        props.value ? props.value.map(x => {

            const { imdbID, Year, Type, Title, Poster } = x;
            const style = {
                height: '15%'
            };
            return (
                <div class="col-lg-4 col-sm-6">
                    <div class="thumbnail">
                        <div className="card" style={style}>
                            <img className="card-img-top" src={Poster} alt="Card image cap" />
                            <div className="card-body">
                                <h5 className="card-title">{Title}</h5>
                                <p className="card-text">{Year}</p>
                                {
                                    !x.Fav ?

                                        <a href="#" onClick={() => props.addMovie(imdbID)} className="btn btn-primary">Add</a>
                                        : <a href="#" onClick={() => props.removeMovie(imdbID)} className="btn btn-warning">Remove</a>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            )
        })
            : null
    );

} 