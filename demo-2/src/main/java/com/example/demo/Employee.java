package com.example.demo;

public class Employee {

	private String city;
	private String name;
	private int age ;
	
	

	
	public Employee(String city, String name, int age) {
		super();
		this.city = city;
		this.name = name;
		this.age = age;
	}



	public Employee() {
		super();
	}
	
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}



	public String getCity() {
		return city;
	}



	public void setCity(String city) {
		this.city = city;
	}



	public int getAge() {
		return age;
	}



	public void setAge(int age) {
		this.age = age;
	}
	
	
	
	
	
}
