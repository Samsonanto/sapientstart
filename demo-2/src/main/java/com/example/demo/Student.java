package com.example.demo;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;


public class Student {

	private int regno;
	private String fname;
	private String lname;
	private Date dob ;
	private int citycode;
	
	

	public Student() {
		super();
	}
	


	

	public Student(int regno, String fname, String lname, Date dob, int citycode) {
		super();
		this.regno = regno;
		this.fname = fname;
		this.lname = lname;
		this.dob = dob;
		this.citycode = citycode;
	}

	public int getRegno() {
		return regno;
	}

	public void setRegno(int regno) {
		this.regno = regno;
	}

	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public int getCitycode() {
		return citycode;
	}
	public void setCitycode(int citycode) {
		this.citycode = citycode;
	}
	
	
}
