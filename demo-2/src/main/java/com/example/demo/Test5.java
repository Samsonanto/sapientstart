package com.example.demo;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.client.RestTemplate;

public class Test5 {

	static final String URL_CREATE_EMPLOYEE = "http://localhost:8900/Student/{id}";	     
	
	public static void main(String[] args) {
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "3");
		
	    RestTemplate restTemplate = new RestTemplate();
	    restTemplate.delete(URL_CREATE_EMPLOYEE,map);
		
	    System.out.println("Done");
	    
	}
	
}
