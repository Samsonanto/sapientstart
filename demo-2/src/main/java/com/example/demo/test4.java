package com.example.demo;


import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class test4 {

	static final String URL_CREATE_EMPLOYEE = "http://localhost:8900/updateStudent";
	public static void main(String[] args) {
		
		MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
		
		map.add("regno", "1");
		map.add("fname", "Samson");
		map.add("lname", "Anto");
		map.add("dob", "1997-11-07");
		map.add("citycode", "1");
		
	    
	    HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
	    HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<MultiValueMap<String, String>>(map, headers);
	    
	    
	    RestTemplate restTemplate = new RestTemplate();
	    restTemplate.put ( URL_CREATE_EMPLOYEE, entity);
		System.out.println("Done");
		
	}
}
