package com.example.demo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CityBean {

	@Id
	private int cityCode;
	@Column
	private String cityaName;
	
		
	
	public CityBean() {
		super();
	}
	public CityBean(int cityCode, String cityaName) {
		super();
		this.cityCode = cityCode;
		this.cityaName = cityaName;
	}
	public int getCityCode() {
		return cityCode;
	}
	public void setCityCode(int cityCode) {
		this.cityCode = cityCode;
	}
	public String getCityaName() {
		return cityaName;
	}
	public void setCityaName(String cityaName) {
		this.cityaName = cityaName;
	}
	
	
	
	
	
	
	
}
