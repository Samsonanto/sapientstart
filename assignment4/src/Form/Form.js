import React from "react";
import "./Form.css"

const Form = (props) => {
    return (

        <div className="Form">
            <form>
                <div className="form-group">
                    <label htmlFor="exampleInputPassword1">Name</label>
                    <input type="text" onChange={props.nameChange} value={props.name} className="form-control" id="exampleInputPassword1" placeholder="Name" aria-describedby="help1" />
                    <small id="help1" className="form-text text-muted">{props.errorName}</small>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleInputEmail1">Email address</label>
                    <input type="email" onChange={props.emailChange} value={props.email} className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
                    <small className="form-text text-muted">{props.errorEmail}</small>
                </div>
                <div className="form-group">
                    <label htmlFor="inputGroupSelect01">City</label>
                    <select class="custom-select" id="inputGroupSelect01" value={props.city} onChange={props.cityChange}>
                        <option selected value="1">Choose Your City</option>
                        <option value="Delhi">Delhi</option>
                        <option value="Mumbai">Mumbai</option>
                        <option value="Gurugram">Gurugram</option>
                    </select>
                    <small className="form-text text-muted">{props.errorCity}</small>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleInputPassword1">DOB</label>
                    <input type="Date" onChange={props.dobChange} value={props.DOB} className="form-control" id="exampleInputPassword1" placeholder="Date" />
                    <small className="form-text text-muted">{props.errorDOB}</small>
                </div>
                <button onSubmit={props.onSubmit} type="submit" className="btn btn-primary">Submit</button>
            </form>

        </div>


    );
}

export default Form;