import React from 'react';
import Form from './Form/Form'
import './App.css';
import Navbar from './Navbar/Navbar';

class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      email: "",
      city: "1",
      DOB: "",
      name: "",
      errorName: "",
      errorCity: "",
      errorEmail: "",
      errorDOB: ""
    }

  }

  emailChange = (event) => {


    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (!re.test(String(event.target.value).toLowerCase())) {
      this.setState({
        errorEmail: "Enter a Valid Email"

      });
    }
    else{
      this.setState({
        errorEmail: ""

      });
    }
    this.setState({

      email: event.target.value

    })
    console.log(this.state);
  }

  cityChange = (event) => {


    switch(event.target.value){
      case 'Delhi':
      case 'Mumbai':
      case 'Gurugram':
        this.setState({errorCity : ""})
      default:
        this.setState({errorCity : "not a valid city"})
    }

    this.setState({

      city: event.target.value

    });
    console.log(this.state);
  }
  dobChange = (event) => {

    this.setState({

      DOB: event.target.value

    });
    console.log(this.state);
  }
  nameChange = (event) => {

    var re = /^[a-z0-9]+$/i

    if(event.target.value.length < 3 || !re.test(String(event.target.value).toLowerCase())){
      this.setState({ errorName : "Invalid name"});
    }
    else{
      this.setState({errorName : ""});
    }

    this.setState({

      name: event.target.value

    });
    console.log(this.state);
  }


  onSubmit=(event)=>{

    event.preventDefault();

  }

  render() {
    return (<div className="App">

      <Navbar></Navbar>

      <Form
        name={this.state.name} nameChange={this.nameChange} errorName={this.errorName}
        email={this.state.email} emailChange={this.emailChange} errorEmail={this.state.errorEmail}
        DOB={this.state.DOB} dobChange={this.dobChange} errorDOB={this.state.errorDOB}
        city={this.state.city} cityChange={this.cityChange} errorCity={this.state.errorCity}
        onSubmit={this.onSubmit}
      ></Form>
    </div>
    );
  }
}

export default App;
