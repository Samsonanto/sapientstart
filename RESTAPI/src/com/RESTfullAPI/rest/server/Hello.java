package com.RESTfullAPI.rest.server;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/hello77")
public class Hello {
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getHello() {
		return "Hello";
	}
}
