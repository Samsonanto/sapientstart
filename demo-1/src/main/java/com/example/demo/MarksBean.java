package com.example.demo;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="Marks")
public class MarksBean {

	@Id
	private int id;
	
	private int regno;
	@Column
	private String testno;
	@Column
	private int marks1;
	@Column
	private int marks2;
	@Column
	private int marks3;
	
	@Transient
	@OneToMany()
	@JoinColumn(name="regno")
	List<StudentBean> sudentRef;
	
	
	
	public String getTestno() {
		return testno;
	}
	public void setTestno(String testno) {
		this.testno = testno;
	}
	public List<StudentBean> getSudentRef() {
		return sudentRef;
	}
	public void setSudentRef(List<StudentBean> sudentRef) {
		this.sudentRef = sudentRef;
	}
	public MarksBean() {
		super();
	}
	public MarksBean(int id, int regno, int marks1, int marks2, int marks3) {
		super();
		this.id = id;
		this.regno = regno;
		this.marks1 = marks1;
		this.marks2 = marks2;
		this.marks3 = marks3;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRegno() {
		return regno;
	}
	public void setRegno(int regno) {
		this.regno = regno;
	}
	public int getMarks1() {
		return marks1;
	}
	public void setMarks1(int marks1) {
		this.marks1 = marks1;
	}
	public int getMarks2() {
		return marks2;
	}
	public void setMarks2(int marks2) {
		this.marks2 = marks2;
	}
	public int getMarks3() {
		return marks3;
	}
	public void setMarks3(int marks3) {
		this.marks3 = marks3;
	}
	
	
	
	
	
}
