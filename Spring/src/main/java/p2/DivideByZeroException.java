package p2;

public class DivideByZeroException extends Exception {

	DivideByZeroException(String msg){
		super(msg);
	}
	DivideByZeroException(){
		super();
	}
	
}
