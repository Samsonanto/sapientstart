package p2;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class AurthAspects {

	@Before( "execution(* *.add(float,float))")
	public void checkpoint(JoinPoint jp) {
		for(Object x : jp.getArgs()) {
			float v = (Float)x;
			if(v<0) {
				throw new IllegalArgumentException("Number cannot be -ve");
			}
		}
	}
	@AfterReturning(pointcut = "execution(* *.sub(float,float))",returning = "o")
	public void checkpoint2(JoinPoint jp, Object o) {
			
		float v = (Float)o; 
			if(v<0) {
				throw new IllegalArgumentException("Result should be grater than 0");
		}
	}
}
