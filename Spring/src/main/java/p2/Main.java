package p2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
	public static void main(String[] args) {
		Aurthamatics ob;
		
		ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
		
		
		ob = (Aurthamatics) context.getBean("arith");
		
		

		try {
			
			float res = ob.sub(5,9);
			System.out.println(res);
		}catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		
	}
}
