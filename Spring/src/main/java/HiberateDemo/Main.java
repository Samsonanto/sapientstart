package HiberateDemo;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String[] args) {
		
		
		ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
		
		System.out.println("Maven + Hibernate + MySQL");
        Session session =  Util.sessionFactory();
        
        
        session.beginTransaction();
        

//        EmployeeDAO.insertEmployee(new Employee("Joel","100000","19","joel@gmail.com"));
//        
//       session.getTransaction().commit();
        
      List<Employee> e = EmployeeDAO.getAllEmployee();  
        
      for(Employee e1: e) {
    	  System.out.println(e1.toString());
      }
      
      
      try {
    	  EmployeeDAO.updateSalaryById("2",session);
    	  session.getTransaction().commit();
    	  
      }catch(InvalidSalaryUpdateException e1)
      {
    	System.out.println("Error : "+e1.getMessage());
    	session.getTransaction().rollback();
      }
      
      
      session.close();
      System.out.println("End");
	}
	
}
