package HiberateDemo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Util {

//	private static final SessionFactory sessionFactory = buildSessionFactory();
	static Session s = null; 

    public static Session sessionFactory() {
    	
    	if(s == null)
    	{
	        try {
	            // Create the SessionFactory from hibernate.cfg.xml
	            s= new Configuration().configure().buildSessionFactory().openSession();
	        }
	        catch (Throwable ex) {
	            // Make sure you log the exception, as it might be swallowed
	            System.err.println("Initial SessionFactory creation failed." + ex);
	            throw new ExceptionInInitializerError(ex);
	        }
    	}
    	return s;
    }

//    public static SessionFactory getSessionFactory() {
//        return sessionFactory;
//    }
//    
//    public static void shutdown() {
//    	// Close caches and connection pools
//    	getSessionFactory().close();
//    }
}
