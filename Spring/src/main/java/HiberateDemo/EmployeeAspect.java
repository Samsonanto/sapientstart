package HiberateDemo;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
@Aspect
@Order(0)
public class EmployeeAspect {
	
	@AfterReturning(pointcut = "execution(* *.*(*))",returning = "o")
	public void checkpoint(JoinPoint jp, Object o) throws InvalidSalaryUpdateException {
			System.out.println("Inside Aspect");
		float v = (Float)o; 
			if(v>70000) {
				throw new InvalidSalaryUpdateException("Salary cannot be more than 70k");
		}
	}
	
	

}
