package HiberateDemo;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class EmployeeDAO {

	private static List<Employee> emp; 
	private static Session session = Util.sessionFactory();
	public static List<Employee> getAllEmployee() {
		
		 
		
	    Query query=session.createQuery("from Employee");//here persistent class name is Emp  
	    emp=query.list();  
		
		return emp;
		
	}
	
	public static float updateSalaryById(String id, Session session ) throws InvalidSalaryUpdateException {
		
		try {
			
		
		
		Query q=session.createQuery("select sal from Employee where id=:i");
		q.setParameter("i",id);  
		
		List<String> sal = q.list();
		
		
		float inc = Float.parseFloat(sal.get(0)) + 10000.0f;
		System.out.println("sal float "+ inc);
		
		q=session.createQuery("update Employee set sal= :n where id=:i");  
		q.setParameter("n",""+inc);  
		q.setParameter("i",id);  
		q.executeUpdate();
		
		return inc;
		
		}
catch(Exception e) {
			throw new InvalidSalaryUpdateException(e.getMessage());
}
	
	}
	
	public static void insertEmployee(Employee e) {
		session.save(e);
	}
}
