package Demo;

import java.util.List;

public class ListHoliday {

	List<Holiday> holidays;

	public ListHoliday(List<Holiday> holidays) {
		super();
		this.holidays = holidays;
	}

	public List<Holiday> getHolidays() {
		return holidays;
	}

	public void setHolidays(List<Holiday> holidays) {
		this.holidays = holidays;
	}

	public ListHoliday() {
		super();
	}

	
	
	
}
