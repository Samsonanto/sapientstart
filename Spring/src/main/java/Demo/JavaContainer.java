package Demo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JavaContainer {
	
	@Bean
	public Dislpay get1() {
		return new Dislpay(); //Default scope is singleton 
	}
	@Bean
	public Employee get2() {
		return new Employee(21,"Samson"); //Default scope is singleton 
	}
	@Bean
	public Holiday get3() {
		return new Holiday("Wed",new Date("7/12/19")); //Default scope is singleton 
	}
	@Bean
	public Holiday get4() {
		return new Holiday("Wed",new Date("7/2/19")); //Default scope is singleton 
	}
	@Bean
	public Holiday get5() {
		return new Holiday("Wed",new Date("7/7/19")); //Default scope is singleton 
	}
	@Bean
	public ListHoliday get6() {
		
		List<Holiday> lh =new ArrayList<Holiday>();
		
		lh.add(new Holiday("Wed",new Date("7/1/19") ));
		lh.add(new Holiday("Wed",new Date("7/12/19") ));
		lh.add(new Holiday("Wed",new Date("7/2/19") ));
		
		return new ListHoliday(lh); //Default scope is singleton 
	}
	
	
	
}
