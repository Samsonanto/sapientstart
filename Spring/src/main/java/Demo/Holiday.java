package Demo;

import java.util.Date;

public class Holiday {
	
	private String day;
	private Date date;
	public Holiday(String day, Date date) {
		super();
		this.day = day;
		this.date = date;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Holiday() {
		super();
	}
	@Override
	public String toString() {
		return "Holiday [day=" + day + ", date=" + date + "]";
	}
		
	
}
