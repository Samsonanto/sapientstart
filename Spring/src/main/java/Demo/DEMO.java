package Demo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DEMO {

	public static void main(String[] args) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
		Dislpay dp;
		
		dp = (Dislpay)context.getBean("hello");
		dp.display();
		
		
		ListHoliday lh;
		
		lh = (ListHoliday)context.getBean("list");
		for(Holiday h:lh.getHolidays()) {
			System.out.println(h.toString());
			
		}
		
		Employee e = (Employee)context.getBean("emp");
		
		System.out.println(e.toString());
		
	}
	
}
