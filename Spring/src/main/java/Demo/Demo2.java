package Demo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo2 {
	public static void main(String[] args) {
		ApplicationContext context = new AnnotationConfigApplicationContext(JavaContainer.class);	
	
		Dislpay dp = (Dislpay)context.getBean(Dislpay.class);
		
		dp.display();
		Employee e = (Employee)context.getBean(Employee.class);
		System.out.println(e.toString());
		
		ListHoliday lh;
		
		lh = (ListHoliday)context.getBean(ListHoliday.class);
		for(Holiday h:lh.getHolidays()) {
			System.out.println(h.toString());
			
		}
	}
}
