package com.example.demo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="users")
public class UserBean {

	@Id
	private int id;
	@Column
	private String username;
	@Column
	private String pass;
	@Column
	private String name;
	@Column
	private String email;
	public UserBean() {
		super();
	}
	public UserBean(int id, String username, String pass, String name, String email) {
		super();
		this.id = id;
		this.username = username;
		this.pass = pass;
		this.name = name;
		this.email = email;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "UserBean [id=" + id + ", username=" + username + ", pass=" + pass + ", name=" + name + ", email="
				+ email + "]";
	}
	
	
	
	
}
