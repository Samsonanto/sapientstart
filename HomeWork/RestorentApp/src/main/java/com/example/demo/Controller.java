package com.example.demo;

import java.sql.PreparedStatement;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class Controller {

	@Autowired
	UserDAO uDAO;
	
	
	@PostMapping("/login")
	public boolean validate(@RequestBody UserBean u) {
		
		
//		Optional<UserBean> l = uDAO.findByUsername(u.getUsername());
		
		
//		if(l.get().getPass().equals(u.getPass()))
//		{
//			return l.get();  
//		}
		Iterable<UserBean> i =uDAO.findByUsername(u.getUsername());
		
		for(UserBean u1 : i) {
			return u1.getPass().equals(u.getPass());
		}
		return false;
	}
	
}
