package com.example.demo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface UserDAO extends CrudRepository<UserBean, Integer> {

	
	@Query("select id,username,email,name,pass from UserBean where username=?1")
	public Iterable<UserBean> findByUsername(String username);
	
}
