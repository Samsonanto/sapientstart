package com.example.demo;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CourseController {

	@Autowired
	CourseDAO dao;

	@GetMapping("/course")
	public List<CourseBean>  getMethod() {

		return (List<CourseBean>)dao.findAll(); 

	}

	@PostMapping("/course")
	public String postMethod(@RequestBody CourseBean s) {
		// TODO Auto-generated method stub

		dao.save(s);

		return "Sucessful";

	}

	@GetMapping("/course/{id}")
	public Optional<CourseBean> putMethod(@PathVariable("id") int id) {
		// TODO Auto-generated method stub
		
		return dao.findById(id);

	}

	@DeleteMapping("/course/{id}")

	public String deleteMethod(@PathVariable("id") int id) {
		// TODO Auto-generated method stub

		dao.deleteById(id);

		return "Deleted";
		
	}



}
