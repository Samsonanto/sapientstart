package Assignment.rest.server;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sapient.student.dao.StudentDAO;

@Path("/student")
public class Student {
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getString() {
		return "Welecome to student DB";
	}
	
	@GET
	@Path("/{id}")
	public Response getDetails(
			@PathParam("id") String id
			) {
		
		System.out.println("Here");
		
		String output = "Invalid ID";
		try {
			output = StudentDAO.getStudentById(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return Response.status(200).entity(output).build();
		
	}
	
	
	
}
