package com.sapient.student.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sapient.student.bean.StudentBean;

/**
 * Servlet implementation class InsertStudent
 */
@WebServlet("/InsertStudent")
public class InsertStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertStudent() {
        super();
        // TODO Auto-generated constructor stub
    }

    ServletContext cn;
    @Override
    public void init(ServletConfig config) throws ServletException {
    	// TODO Auto-generated method stub
    	cn = config.getServletContext();
    	
    	super.init(config);
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(request.getSession().getAttribute("id") == null)
			request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		if(request.getSession().getAttribute("id") == null)
			request.getRequestDispatcher("login.jsp").forward(request, response);
		
		Map<String,StudentBean> map= (Map<String, StudentBean>) cn.getAttribute("smlist");
		
		String id  =  request.getParameter("id");
		String rollno = request.getParameter("rollno");
		String name = request.getParameter("name");
		String marks = request.getParameter("id");
		String pass = request.getParameter("id");
		
		map.put(request.getParameter("id"),new StudentBean(id,name,rollno,marks,pass));
		
		
		request.setAttribute("list", map.values());
		
		request.getRequestDispatcher("insert.jsp").forward(request, response);
		
	}

}
