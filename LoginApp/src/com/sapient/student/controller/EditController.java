package com.sapient.student.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sapient.student.bean.StudentBean;
import com.sun.net.httpserver.HttpContext;

/**
 * Servlet implementation class EditController
 */
@WebServlet("/EditController")
public class EditController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditController() {
        super();
        // TODO Auto-generated constructor stub
    }

   ServletContext cn;
   @Override
public void init(ServletConfig config) throws ServletException {
	// TODO Auto-generated method stub

	   cn =  config.getServletContext();
	   super.init(config);
}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		request.getRequestDispatcher("login.jsp").forward(request, response);
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		if(request.getSession().getAttribute("id") == null)
			request.getRequestDispatcher("login.jsp").forward(request, response);
		
		Map<String,StudentBean> map = (Map<String, StudentBean>) cn.getAttribute("smlist");
		
		StudentBean b = map.get(request.getParameter("id"));
		b.setName(request.getParameter("name"));
		b.setPercent(request.getParameter("marks"));
		b.setPassword(request.getParameter("password"));
		
		request.getRequestDispatcher("insert.jsp").forward(request, response);
		
	}

}
