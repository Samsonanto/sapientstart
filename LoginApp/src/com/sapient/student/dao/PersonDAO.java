package com.sapient.student.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PersonDAO {

	public static boolean validate(String id, String pass) throws SQLException,ClassNotFoundException {
		// TODO Auto-generated method stub
		
		Connection con = DBConnect.getConnection();
		
		PreparedStatement ps =con.prepareStatement("select * from users where id = ? and pass =? ");
		ps.setString(1, id);
		ps.setString(2, pass);
		
		ResultSet rs = ps.executeQuery();
		if(rs.next())
			return true;
		
		return false;
	}

	public static int getAccessRights(String id) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		Connection con = DBConnect.getConnection();
		PreparedStatement ps =con.prepareStatement("select type from users where id = ? ");
		ps.setString(1, id);
		
		ResultSet rs = ps.executeQuery();
		rs.next();
		
		return (int)(rs.getString(1).toCharArray()[0]) -  (int)('0') ;
	}

	
	
}
