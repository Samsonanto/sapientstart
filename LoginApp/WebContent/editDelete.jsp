<%@page import="com.sapient.student.bean.StudentBean"%>
<%@page import="java.util.Map"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<%

HttpSession hs = request.getSession();

if(hs.getAttribute("id") == null){
	request.getRequestDispatcher("login.jsp").forward(request, response);
}

Map<String,StudentBean> map = (Map<String,StudentBean>)request.getAttribute("smlist");
%>
<body>
<center>
<h1>
Edit Delete Module
</h1>
<table border = "1">
<%
for(StudentBean ob : map.values()){
if(ob.getId().equals("ID")){
	continue;
}
%>
<tr>
<td><%=ob.getId() %></td><td><%=ob.getName() %></td><td><%=ob.getRollno() %></td><td><%=ob.getPercent() %></td>
<td><a href=edit.jsp?id1=<%=ob.getId()%>&id2=<%=ob.getName()%>&id3=<%=ob.getRollno()%>&id4=<%=ob.getPercent()%>>Edit</a></td>
<td><a href=delete.jsp?id1=<%=ob.getId()%>&id2=<%=ob.getName()%>&id3=<%=ob.getRollno()%>&id4=<%=ob.getPercent()%>>Delete</a></td>
</tr>
<%
}%>
</table>
</center>
</body>
</html>