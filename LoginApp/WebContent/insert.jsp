<%@page import="java.util.Collection"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="com.sapient.student.bean.StudentBean"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>

<% 

if(request.getSession().getAttribute("id") == null){
	request.getRequestDispatcher("login.jsp").forward(request,response);
}

%>


<body>
<center>
	<form action = "InsertStudent" method="post" >
	
		ID : <input name="id" type= "number" required><br>		
		Roll No. : <input name="rollno" type= "number" required><br>
		Name : <input name="name" type= "text" required><br>
		Marks : <input type= "number" required><br>
		Password : <input type= "password" required><br>
		<input type = "submit" value="Insert" >
		
	</form>
	<form action="ListStudent" method = "get">
		<input type="submit" value="List Student" >
	</form>
	
	
	
	<form action = "LogoutController" method="get" >
		<input type = "submit" value="Logout" >
	</form> 
	<% 
	
	Object o = request.getAttribute("list"); 
	if( o != null ) {
		List<StudentBean> l = new ArrayList<StudentBean>((Collection<StudentBean>)o);
	%>
	<h3>List of Student</h3>
	
	
	<table>
	
	
		<%for(StudentBean b : l){ 
		
			if(!b.getId().equals("id")){
		%>
			<tr>
			<td><%=b.getId() %></td>
			<td><%=b.getName() %></td>
			<td>
			<a href="/LoginApp/edit.jsp?id=<%=b.getId()%>&name=<%=b.getName()%>&rollno=<%=b.getRollno()%>&marks=<%=b.getPercent()%>" > Edit</a>
			
			</td>
			<td>
			<a href="/LoginApp/DeleteController="+<%= b.getId() %>>Delete</a>
			</td>
			</tr>
		<%}} %>
	
	</table>
	
	<%} %>
	
</center>
</body>
</html>